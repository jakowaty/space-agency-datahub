# Zadanie

### INSTALACJA
1. Należy zaimportować bieżący katalog jako Projekt do IntelliJ Ultimate
(wybierając opcję "Import as Maven project")

2. Z konsoli Maven wykonać:  ``mvn spring-boot:run``, projekt uruchomi się pod adresem localhost:8080

3. Zgrany jest plik z requestami do Postmana 

## Endpointy

#### a) PRODUCT

1. _POST /api/product_  
_RequestBody:_ {"url":"http://example.com/prod/1"}
_Code_ : 201

2. _PUT /api/product/{id}_  
_RequestBody:_ {
	"url":"http://name.pl/p/1",
	"price": 125,
	"missionName":"Mission 1",
	"coordinates": {
		"x1": 10.12,
		"x2": 12.12,
		"y1": 10.11,
		"y2": 14.10
	}
}
_Code_ : 202

3. _GET_ /api/product

_RequestBody:_ brak
_Code_ : 200

4. _GET_ /api/product/{id}

_RequestBody:_ brak
_Code_ : 200

4. _DELETE_ /api/product/{id}

_RequestBody:_ brak
_Code_ : 204

#### b) ORDER
1. _POST /api/order_  
_RequestBody:_ {
               	"customerName":"Customer1",
               	"product": [
               	    {
                       	"id": 1,
                       	"missionName": "Mission 1",
                       	"acquisitionDate": "2020-03-16T13:46:52.560068+01:00",
                       	"price": 125,
                       	"coordinates": {
                           	"id": 2,
                           	"x1": 10.12,
                           	"x2": 12.12,
                           	"y1": 10.11,
                           	"y2": 14.1
                       	},
                       	"url": "http://name.pl/p/1"
                   	}
               	]
               }  
_Code_ : 201

#### b) MISSION

1. _POST /api/mission_  
_RequestBody:_ {
               	"name": "Mission 2",
               	"type":"HYPERSPECTRAL"
               }
_Code :_ 201               

2. _GET_ /api/mission

_RequestBody:_ brak  
_Code_ : 200

## Użytkownicy:
admin//password1  
user1//password1

#Pozostało do zaimplementowania:
 1. Dokończyć konfigurację SpringBoot Security
 2. Wyszukiwanie
 3. Testy jednostkowe