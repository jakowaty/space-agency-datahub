package com.recruitment.spaceagency.datahub.controller;

import com.recruitment.spaceagency.datahub.entity.Mission;
import com.recruitment.spaceagency.datahub.entity.MissionType;
import com.recruitment.spaceagency.datahub.exception.EntityNotFoundException;
import com.recruitment.spaceagency.datahub.service.MissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/mission")
public class MissionController {

    @Autowired
    private MissionService missionService;

    public MissionService getMissionService() {
        return missionService;
    }

    public void setMissionService(MissionService missionService) {
        this.missionService = missionService;
    }


    @GetMapping(produces = "application/json")
    public List<Mission> list() {
        return missionService.list();
    }


    @GetMapping(value = "{/id}", produces = "application/json")
    public Mission get(Long id) {
        try {
            return missionService.get(id);
        } catch (EntityNotFoundException exc) {
            ResponseEntity.status(HttpStatus.NOT_FOUND);
            return null;
        }
    }


    @PostMapping(produces = "application/json")
    public ResponseEntity<Mission> add(@RequestBody Mission mission) {
        missionService.add(mission.getName(), mission.getMissionImagery());
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Mission> update(@PathVariable Long id, @RequestBody Mission mission) {
        try {
            missionService.update(id, mission);
        } catch (EntityNotFoundException exc) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Mission> delete(@PathVariable Long id) {
        try {
            missionService.finish(id);
        } catch (EntityNotFoundException exc) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
