package com.recruitment.spaceagency.datahub.controller;

import com.recruitment.spaceagency.datahub.entity.PlacedOrder;
import com.recruitment.spaceagency.datahub.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/order")
public class OrderController {

    @Autowired
    private CustomerService customerService;


    @PostMapping(produces = "application/json")
    public PlacedOrder add(@RequestBody PlacedOrder order) {
        return customerService.makeAnOrder(order.getCustomerName(), order.getProduct());
    }
}
