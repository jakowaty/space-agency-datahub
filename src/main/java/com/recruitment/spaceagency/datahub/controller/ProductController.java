package com.recruitment.spaceagency.datahub.controller;

import com.recruitment.spaceagency.datahub.entity.MissionType;
import com.recruitment.spaceagency.datahub.entity.Product;
import com.recruitment.spaceagency.datahub.exception.EntityNotFoundException;
import com.recruitment.spaceagency.datahub.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping(produces = "application/json")
    public List<Product> list() {
        return productService.list();
    }

    @GetMapping(value = "{/id}", produces = "application/json")
    public Product get(@PathVariable Long id) {
        try {
            return productService.get(id);
        } catch (EntityNotFoundException exc) {
            ResponseEntity.status(HttpStatus.NOT_FOUND);
            return null;
        }
    }

    @GetMapping(value = "/search")
    public List<Product> search(
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "mission-product-type", required = false) String type,
            @RequestParam(name = "acquisition-date-from", required = false) OffsetDateTime acquisitionDtaeFrom,
            @RequestParam(name = "acquisition-date-to", required = false) OffsetDateTime acquisitionDtaeTo
    ) {

        if (name != null) {
            return productService.getByMissionName(name);
        }

        if (type != null) {
            return productService.getByMissionType(MissionType.valueOf(type));
        }

        if (acquisitionDtaeFrom != null && acquisitionDtaeTo != null) {
            return productService.getByAcquisitionDate(acquisitionDtaeFrom, acquisitionDtaeTo);
        }

        return Collections.emptyList();
    }

    @PostMapping(produces = "application/json")
    public ResponseEntity<Product> add(@RequestBody String url) {
        productService.add(url);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Product> update(@PathVariable Long id, @RequestBody Product product) {
        try {
            productService.update(id, product);
        } catch (EntityNotFoundException exc) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Product> delete(@PathVariable Long id) {
        try {
            productService.delete(id);
        } catch (EntityNotFoundException exc) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
