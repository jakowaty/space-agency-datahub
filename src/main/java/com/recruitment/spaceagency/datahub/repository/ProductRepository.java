package com.recruitment.spaceagency.datahub.repository;

import com.recruitment.spaceagency.datahub.entity.Product;
import org.springframework.data.repository.CrudRepository;

import java.time.OffsetDateTime;
import java.util.List;

public interface ProductRepository extends CrudRepository <Product, Long> {
    List<Product> findByMissionName(String missionName);
    List<Product> findAllByAcquisitionDateBetween(OffsetDateTime from, OffsetDateTime to);
}
