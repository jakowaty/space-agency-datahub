package com.recruitment.spaceagency.datahub.repository;

import com.recruitment.spaceagency.datahub.entity.Mission;
import com.recruitment.spaceagency.datahub.entity.MissionType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface MissionRepository extends CrudRepository<Mission, Long> {
    public Optional<Mission> findMissionByName(String name);
    public List<Mission> findAllByMissionImagery(MissionType type);
}
