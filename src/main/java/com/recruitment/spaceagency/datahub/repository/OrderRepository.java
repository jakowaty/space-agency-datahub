package com.recruitment.spaceagency.datahub.repository;

import com.recruitment.spaceagency.datahub.entity.PlacedOrder;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository <PlacedOrder, Long> {
}
