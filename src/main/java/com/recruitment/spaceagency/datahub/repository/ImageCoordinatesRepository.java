package com.recruitment.spaceagency.datahub.repository;

import com.recruitment.spaceagency.datahub.entity.ImageCoordinates;
import org.springframework.data.repository.CrudRepository;

public interface ImageCoordinatesRepository extends CrudRepository <ImageCoordinates, Long> {
}
