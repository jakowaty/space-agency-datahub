package com.recruitment.spaceagency.datahub.entity;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
public class Product {
    @Id
    @GeneratedValue
    private long id;

    @Column(length = 255)
    private String missionName;

    private OffsetDateTime acquisitionDate;

    private long price;

    @OneToOne
    private ImageCoordinates coordinates;

    @Column(length = 255)
    private String url;

    public Product() {
        setUrl("");
    }

    public Product(String url) {
        setUrl(url);
    }


    public long getId() {
        return id;
    }

    public String getMissionName() {
        return missionName;
    }

    public void setMissionName(String missionName) {
        this.missionName = missionName;
    }

    public OffsetDateTime getAcquisitionDate() {
        return acquisitionDate;
    }

    public void setAcquisitionDate(OffsetDateTime acquisitionDate) {
        this.acquisitionDate = acquisitionDate;
    }

    public ImageCoordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ImageCoordinates coordinates) {
        this.coordinates = coordinates;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
