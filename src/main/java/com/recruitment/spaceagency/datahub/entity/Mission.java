package com.recruitment.spaceagency.datahub.entity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;

@Entity
public class Mission {
    public Mission() {
        this("");
    }

    public Mission(String name) {
        this.setName(name);
        this.setMissionImagery(MissionType.HYPERSPECTRAL);
    }

    public Mission(String name, MissionType missionImagery) {
        this.setName(name);
        this.setMissionImagery(missionImagery);
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column(length = 255, unique = true)
    private String name;

    @Enumerated(EnumType.STRING)
    private MissionType missionImagery;

    private OffsetDateTime startDate;

    private OffsetDateTime endDate;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MissionType getMissionImagery() {
        return missionImagery;
    }

    public void setMissionImagery(MissionType missionImagery) {
        this.missionImagery = missionImagery;
    }

    public OffsetDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(OffsetDateTime startDate) {
        this.startDate = startDate;
    }

    public OffsetDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(OffsetDateTime endDate) {
        this.endDate = endDate;
    }
}
