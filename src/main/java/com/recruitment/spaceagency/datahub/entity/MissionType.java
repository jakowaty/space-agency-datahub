package com.recruitment.spaceagency.datahub.entity;

public enum MissionType {
    PANCHROMATIC("Panchromatic"),
    MULTISPECTRAL("Multispectral"),
    HYPERSPECTRAL("Hyperspectral");

    private final String type;

    MissionType(String missionType) {
        type = missionType;
    }

    public String getType() {
        return type;
    }
}
