package com.recruitment.spaceagency.datahub.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class PlacedOrder {
    @Id
    @GeneratedValue
    private long id;


    @ManyToMany
    private Set<Product> product;

    @Column(length = 100)

    private String customerName;

    public Set<Product> getProduct() {
        return product;
    }

    public void setProduct(Set<Product> product) {
        this.product = product;
    }

    public String getCustomerName() {
        return customerName;
    }

    public PlacedOrder setCustomerName(String customerName) {
        this.customerName = customerName;
        return this;
    }
}
