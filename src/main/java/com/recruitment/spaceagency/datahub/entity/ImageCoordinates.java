package com.recruitment.spaceagency.datahub.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ImageCoordinates {

    @Id
    @GeneratedValue
    private Long id;

    private final double x1;
    private final double x2;
    private final double y1;
    private final double y2;

    public ImageCoordinates() {
        this(0.0, 0.0, 0.0, 0.0);
    }

    public ImageCoordinates(double x1, double x2, double y1, double y2) {
        this.x1 = x1;
        this.x2 = x2;

        this.y1 = y1;
        this.y2 = y2;
    }

    public Long getId() {
        return id;
    }

    public double getX1() {
        return x1;
    }

    public double getX2() {
        return x2;
    }

    public double getY1() {
        return y1;
    }

    public double getY2() {
        return y2;
    }
}
