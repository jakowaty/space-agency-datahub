package com.recruitment.spaceagency.datahub.service;

import com.recruitment.spaceagency.datahub.exception.EntityNotFoundException;

import java.util.List;

public interface EntityService <T,V> {
    public T get(Long id) throws EntityNotFoundException;
    public T add(V param);
    public T update(Long id, T param) throws Exception, EntityNotFoundException;
    public List<T> list();
    public void delete(Long id) throws EntityNotFoundException;
}
