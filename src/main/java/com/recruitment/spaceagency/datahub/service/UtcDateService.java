package com.recruitment.spaceagency.datahub.service;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@Service
public class UtcDateService {
    public OffsetDateTime now(ZoneOffset offset) {
        LocalDateTime localDateTime = LocalDateTime.now();

        return OffsetDateTime.of(localDateTime, offset);
    }
}
