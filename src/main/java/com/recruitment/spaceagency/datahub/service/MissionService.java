package com.recruitment.spaceagency.datahub.service;

import com.recruitment.spaceagency.datahub.entity.Mission;
import com.recruitment.spaceagency.datahub.entity.MissionType;
import com.recruitment.spaceagency.datahub.entity.Product;
import com.recruitment.spaceagency.datahub.exception.EntityNotFoundException;
import com.recruitment.spaceagency.datahub.repository.MissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class MissionService implements EntityService <Mission, String> {

    @Autowired
    private MissionRepository missionRepository;

    @Autowired UtcDateService dateService;

    @Override
    public List<Mission> list() {
        List<Mission> resultList = new java.util.ArrayList<>(Collections.emptyList());
        Iterable<Mission> fromRepositoryList = missionRepository.findAll();
        for (Mission misssion : fromRepositoryList) {
            resultList.add(misssion);
        }

        return resultList;
    }

    public Mission get(Long id) throws EntityNotFoundException {
        Optional<Mission> fromDatabase = missionRepository.findById(id);

        if (!fromDatabase.isPresent()) {
            throw new EntityNotFoundException();
        }

        return fromDatabase.get();
    }

    @Override
    public Mission add(String name) {
        Mission mission = new Mission(name);

        mission.setStartDate(dateService.now(ZoneOffset.UTC));

        return missionRepository.save(mission);
    }

    public Mission add(String name, MissionType missionType) {
        Mission mission = add(name);

        mission.setMissionImagery(missionType);


        return missionRepository.save(mission);
    }

    @Override
    public Mission update(Long id, Mission fromRequest) throws EntityNotFoundException {
        Mission fromDatabase = get(id);

        fromDatabase.setMissionImagery(fromRequest.getMissionImagery());
        fromDatabase.setName(fromRequest.getName());

        return missionRepository.save(fromDatabase);
    }

    public Mission finish(Long id) throws EntityNotFoundException {
        Mission fromDatabase = get(id);

        fromDatabase.setEndDate(dateService.now(ZoneOffset.UTC));
        return missionRepository.save(fromDatabase);
    }

    @Override
    public void delete(Long id) throws EntityNotFoundException {
        Mission fromDatabase = get(id);

        missionRepository.delete(fromDatabase);
    }
}
