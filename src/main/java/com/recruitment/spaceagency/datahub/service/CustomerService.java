package com.recruitment.spaceagency.datahub.service;

import com.recruitment.spaceagency.datahub.entity.PlacedOrder;
import com.recruitment.spaceagency.datahub.entity.Product;
import com.recruitment.spaceagency.datahub.exception.EntityNotFoundException;
import com.recruitment.spaceagency.datahub.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class CustomerService {
    @Autowired
    private ProductService productService;

    @Autowired
    OrderRepository orderRepository;

    public CustomerService setProductService(ProductService productService) {
        this.productService = productService;
        return this;
    }

    public CustomerService setOrderRepository(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
        return this;
    }

    public PlacedOrder makeAnOrder(String customerName, Set<Product> products) {
            PlacedOrder order = new PlacedOrder();

            order.setProduct(products);
            order.setCustomerName(customerName);

            return orderRepository.save(order);
    }
}
