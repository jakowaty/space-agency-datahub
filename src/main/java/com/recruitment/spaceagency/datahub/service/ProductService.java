package com.recruitment.spaceagency.datahub.service;

import com.recruitment.spaceagency.datahub.entity.Mission;
import com.recruitment.spaceagency.datahub.entity.MissionType;
import com.recruitment.spaceagency.datahub.entity.Product;
import com.recruitment.spaceagency.datahub.exception.EntityNotFoundException;
import com.recruitment.spaceagency.datahub.repository.ImageCoordinatesRepository;
import com.recruitment.spaceagency.datahub.repository.MissionRepository;
import com.recruitment.spaceagency.datahub.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService implements EntityService<Product, String> {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private MissionRepository missionRepository;

    @Autowired
    ImageCoordinatesRepository imageCoordinatesRepository;

    @Autowired
    private UtcDateService dateService;


    public ProductService setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
        return this;
    }

    public ProductService setMissionRepository(MissionRepository missionRepository) {
        this.missionRepository = missionRepository;
        return this;
    }

    public ProductRepository getProductRepository() {
        return productRepository;
    }

    public MissionRepository getMissionRepository() {
        return missionRepository;
    }

    public UtcDateService getDateService() {
        return dateService;
    }

    public void setDateService(UtcDateService dateService) {
        this.dateService = dateService;
    }


    @Override
    public List<Product> list() {
        List<Product> resultList = new java.util.ArrayList<>(Collections.emptyList());
        Iterable<Product> fromRepositoryList = productRepository.findAll();
         for (Product product : fromRepositoryList) {
             resultList.add(product);
         }

         return resultList;
    }


    @Override
    public Product get(Long id) throws EntityNotFoundException {
        Optional<Product> fromDatabase = productRepository.findById(id);

        if (!fromDatabase.isPresent()) {
            throw new EntityNotFoundException();
        }

        return fromDatabase.get();
    }


    @Override
    public Product add(String url) {
        Product product = new Product(url);

        product.setAcquisitionDate(dateService.now(ZoneOffset.UTC));
        return productRepository.save(product);
    }


    public Product add(String url, int price) {
        Product product = add(url);

        product.setPrice(price);
        return productRepository.save(product);
    }


    @Override
    public Product update(Long id, Product fromRequest) throws EntityNotFoundException {
        Product fromDatabase = get(id);

        fromDatabase.setUrl(fromRequest.getUrl());
        fromDatabase.setMissionName(fromRequest.getMissionName());
        fromDatabase.setPrice(fromRequest.getPrice());

        fromDatabase.setCoordinates(
            imageCoordinatesRepository.save(fromRequest.getCoordinates())
        );

        return productRepository.save(fromDatabase);
    }


    @Override
    public void delete(Long id) throws EntityNotFoundException {
        Product product = get(id);

        productRepository.delete(product);
    }


    public List<Product> getByMissionName(String name) {
        return productRepository.findByMissionName(name);
    }

    public List<Product> getByMissionType(MissionType type) {
        List<Product> productList = new ArrayList<>();
        Iterable<Product> allProducts = productRepository.findAll();

        for (Product prod : allProducts) {
            if (prod.getMissionName() == null) {
                continue;
            }

            Optional<Mission> optionalMission = missionRepository.findMissionByName(prod.getMissionName());

            if (optionalMission.isPresent() &&
                    optionalMission.get().getMissionImagery().getType().equals(prod.getMissionName())) {
                productList.add(prod);
            }

        }

        return productList;

    }

    public List<Product> getByAcquisitionDate(OffsetDateTime from, OffsetDateTime to) {
        return productRepository.findAllByAcquisitionDateBetween(from, to);
    }
}
