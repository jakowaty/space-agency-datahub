package com.recruitment.spaceagency.datahub.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class ApiSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.anonymous().authorities("ROLE_ANON")
        .and()
        .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/h2-console/**").permitAll()


        .and()
        //mission
        .authorizeRequests()
            .antMatchers(HttpMethod.POST, "/api/mission")
            .hasAuthority("PRODUCT_CONTENT_ADMINISTRATOR")


//        //products
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/api/product")
                .hasAuthority("PRODUCT_CONTENT_ADMINISTRATOR")
//
//        .and()
//        .authorizeRequests()
//            .antMatchers("/api/mission/*", "/api/mission/**")
//            .hasAuthority("PRODUCT_CONTENT_ADMINISTRATOR")
////
//        .and()
//        .authorizeRequests()
//            .antMatchers("/api/product/*", "/api/product/**")
//            .hasAuthority("PRODUCT_CONTENT_ADMINISTRATOR")

//
//
//        .and()
//        .authorizeRequests()
//            .antMatchers(HttpMethod.DELETE, "/api/mission/**")
//            .hasAuthority("PRODUCT_CONTENT_ADMINISTRATOR")
//


//        .and()
//        .authorizeRequests()
//            .antMatchers(HttpMethod.DELETE, "/api/product/**")
//            .hasAuthority("PRODUCT_CONTENT_ADMINISTRATOR")
//        .and()
//        .authorizeRequests()
//            .antMatchers(HttpMethod.GET, "/api/search")
//            .permitAll()
//        .and()
//        .authorizeRequests()
//            .antMatchers(HttpMethod.GET, "api/product/**")
//            .permitAll()


        .and()
        .authorizeRequests()
            .antMatchers(HttpMethod.GET, "/api/mission")
            .permitAll()

        .and()
        .authorizeRequests()
            .antMatchers(HttpMethod.GET, "/api/product")
            .permitAll()
//
        .and()
        .httpBasic()
        ;

        http.csrf().disable();
        http.headers().frameOptions().disable();
    }

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin")
                .password("{noop}password1")
                .roles("PRODUCT_CONTENT_ADMINISTRATOR", "CUSTOMER")
                .authorities("PRODUCT_CONTENT_ADMINISTRATOR", "CUSTOMER")
        ;

        auth.inMemoryAuthentication()
                .withUser("user")
                .password("{noop}password1")
                .roles("CUSTOMER")
                .authorities("CUSTOMER")
        ;
    }

}
